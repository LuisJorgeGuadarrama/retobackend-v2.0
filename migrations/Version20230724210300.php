<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230724210300 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE evento ADD id INT AUTO_INCREMENT NOT NULL, DROP idevento, CHANGE Nombre nombre VARCHAR(255) NOT NULL, CHANGE Fecha fecha VARCHAR(255) NOT NULL, CHANGE Lugar lugar VARCHAR(255) NOT NULL, CHANGE Hora hora VARCHAR(255) NOT NULL, CHANGE Categoria categoria VARCHAR(255) NOT NULL, CHANGE Estatus estatus VARCHAR(255) NOT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE usuario ADD id INT AUTO_INCREMENT NOT NULL, DROP idusuario, CHANGE Nombre nombre VARCHAR(255) NOT NULL, CHANGE Apellido apellido VARCHAR(255) NOT NULL, CHANGE Alias alias VARCHAR(255) NOT NULL, CHANGE Correo correo VARCHAR(255) NOT NULL, CHANGE Contraseña contraseña VARCHAR(255) NOT NULL, CHANGE Rol rol VARCHAR(255) NOT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE evento ADD idevento INT UNSIGNED AUTO_INCREMENT NOT NULL, DROP id, CHANGE nombre Nombre VARCHAR(50) NOT NULL, CHANGE fecha Fecha VARCHAR(50) NOT NULL, CHANGE lugar Lugar VARCHAR(50) NOT NULL, CHANGE hora Hora VARCHAR(50) NOT NULL, CHANGE categoria Categoria VARCHAR(50) NOT NULL, CHANGE estatus Estatus VARCHAR(50) NOT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (idevento)');
        $this->addSql('ALTER TABLE usuario ADD idusuario INT UNSIGNED AUTO_INCREMENT NOT NULL, DROP id, CHANGE nombre Nombre VARCHAR(50) NOT NULL, CHANGE apellido Apellido VARCHAR(50) DEFAULT NULL, CHANGE alias Alias VARCHAR(50) NOT NULL, CHANGE correo Correo VARCHAR(50) NOT NULL, CHANGE contraseña Contraseña VARCHAR(50) NOT NULL, CHANGE rol Rol VARCHAR(50) NOT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (idusuario)');
    }
}
