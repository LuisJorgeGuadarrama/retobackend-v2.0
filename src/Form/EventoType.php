<?php

namespace App\Form;

use App\Entity\Evento;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class EventoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        
        $builder
            ->add('Nombre', TextType::class, [
                'label' => 'Nombre',
                'attr' => [
                    'placeholder' => 'Nombre',
                    'autocomplete' => 'off',
                    'class' => 'form-control',
                    'required' => true
                ]
            ])
            ->add('Fecha' , TextType::class, [
                'label' => 'Fecha',
                'attr' => [
                    'placeholder' => 'Fecha',
                    'autocomplete' => 'off',
                    'class' => 'form-control',
                    'required' => true
                ]
            ])
            ->add('Lugar', TextType::class, [
                'label' => 'Lugar',
                'attr' => [
                    'placeholder' => 'Lugar',
                    'autocomplete' => 'off',
                    'class' => 'form-control',
                    'required' => true
                ]
            ])
            ->add('Hora', TextType::class, [
                'label' => 'Hora',
                'attr' => [
                    'placeholder' => 'Hora en formato 24hrs',
                    'autocomplete' => 'off',
                    'class' => 'form-control',
                    'required' => true
                ]
            ])
            ->add('Categoria', TextType::class, [
                'label' => 'Categoria',
                'attr' => [
                    'placeholder' => 'Categoria',
                    'autocomplete' => 'off',
                    'class' => 'form-control',
                    'required' => true
                ]
            ])
            ->add('Estatus',  ChoiceType::class, [
                'choices' => [
                    'Disponible' => 'Disponible',
                    'Cancelado' => 'Cancelado',
                    'Agotado' => 'Agotado',
                    'Poca disponibilidad' => 'Poca disponibilidad',
                ],
            ])

            ->add('submit', SubmitType::class,[
                'label' => 'Guardar',
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Evento::class,
        ]);
    }
}
