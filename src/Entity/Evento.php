<?php

namespace App\Entity;

use App\Repository\EventoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EventoRepository::class)
 */
class Evento
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Fecha;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Lugar;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Hora;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Categoria;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->Nombre;
    }

    public function setNombre(string $Nombre): self
    {
        $this->Nombre = $Nombre;

        return $this;
    }

    public function getFecha(): ?string
    {
        return $this->Fecha;
    }

    public function setFecha(string $Fecha): self
    {
        $this->Fecha = $Fecha;

        return $this;
    }

    public function getLugar(): ?string
    {
        return $this->Lugar;
    }

    public function setLugar(string $Lugar): self
    {
        $this->Lugar = $Lugar;

        return $this;
    }

    public function getHora(): ?string
    {
        return $this->Hora;
    }

    public function setHora(string $Hora): self
    {
        $this->Hora = $Hora;

        return $this;
    }

    public function getCategoria(): ?string
    {
        return $this->Categoria;
    }

    public function setCategoria(string $Categoria): self
    {
        $this->Categoria = $Categoria;

        return $this;
    }

    public function getEstatus(): ?string
    {
        return $this->Estatus;
    }

    public function setEstatus(string $Estatus): self
    {
        $this->Estatus = $Estatus;

        return $this;
    }
}
