<?php

namespace App\Entity;

use App\Repository\UsuarioRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UsuarioRepository::class)
 */
class Usuario
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Apellido;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Alias;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Correo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Contraseña;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Rol;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->Nombre;
    }

    public function setNombre(string $Nombre): self
    {
        $this->Nombre = $Nombre;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->Apellido;
    }

    public function setApellido(string $Apellido): self
    {
        $this->Apellido = $Apellido;

        return $this;
    }

    public function getAlias(): ?string
    {
        return $this->Alias;
    }

    public function setAlias(string $Alias): self
    {
        $this->Alias = $Alias;

        return $this;
    }

    public function getCorreo(): ?string
    {
        return $this->Correo;
    }

    public function setCorreo(string $Correo): self
    {
        $this->Correo = $Correo;

        return $this;
    }

    public function getContraseña(): ?string
    {
        return $this->Contraseña;
    }

    public function setContraseña(string $Contraseña): self
    {
        $this->Contraseña = $Contraseña;

        return $this;
    }

    public function getRol(): ?string
    {
        return $this->Rol;
    }

    public function setRol(string $Rol): self
    {
        $this->Rol = $Rol;

        return $this;
    }
}
