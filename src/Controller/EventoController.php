<?php

namespace App\Controller;

use App\Entity\Evento;
use App\Repository\EventoRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\Json;
use Symfony\Flex\Response;

class EventoController extends AbstractController{
    
    //public function index (){
    //    return $this->render('Usuario/usuario.html.twig');
    //}
    
    public function getevento (EventoRepository $ev,Request $request) 
    { 
       
        $listevento = $ev->findAll();
        $arrayevento = [];
        foreach($listevento as $evento){
            $arrayevento[] =[
                'id'=>$evento->getId(),
                'Nombre'=> $evento->getNombre(),
                'Fecha'=>$evento->getFecha(),
                'Lugar'=>$evento->getLugar(),
                'Hora'=>$evento->getHora(),
                'Categoria'=>$evento->getCategoria(),
                'Estatus'=>$evento->getEstatus()
            ];
        };
       $response = new JsonResponse();
       $response->setData([
        'success'=> true,
        'data'=> $arrayevento
       ]);
       return($response);
    }   

    // App\Controller\ArticleController.php

    public function listAction(EventoRepository $ev, PaginatorInterface $paginator,EntityManagerInterface $em, Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");
        $query = $ev->findAll();

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            5 /*limit per page*/
        );

        // parameters to template
        return $this->render('evento.html.twig', ['pagination' => $pagination]);
    }
    
    public function createevento(Request $request, EntityManagerInterface $em){
    
            $evento = new Evento();
            $form_evento = $this->createForm(\App\Form\EventoType::class, $evento);
            $form_evento->handleRequest($request);
            
            if($form_evento->isSubmitted() && $form_evento->isValid() ){
                $em-> persist($evento);
                $em-> flush();

                return $this->redirectToRoute('getEvento');
            }
    
            return $this-> render('evento_create.html.twig',[
                'form_evento' => $form_evento->createView()
            ]);
        }
    
    public function deleteEvento($id , EntityManagerInterface $em, EventoRepository $eve){
        $evento = $eve ->findOneBy(['id'=>$id]);

        $eve->remove($evento,true);

        return $this->redirectToRoute('getEvento');
    }

    public function modificarEvento($id,Request $request, EntityManagerInterface $em, EventoRepository $eve){
        $evento = $eve ->findOneBy(['id'=>$id]);

        $form_evento = $this->createForm(\App\Form\EventoType::class, $evento);
        $form_evento->handleRequest($request);

        if($form_evento->isSubmitted() && $form_evento->isValid()){
            $em->persist($evento);
            $em->flush();
            
            return $this->redirectToRoute('getEvento');
        
        }
        return $this->render('evento_update.html.twig',[
            'form_evento' => $form_evento->createView()
        ]);

    }
    
}