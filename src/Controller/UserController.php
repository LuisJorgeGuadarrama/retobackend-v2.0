<?php

namespace App\Controller;

use App\Repository\UsuarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Bundle\DoctrineBundle;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class UserController extends AbstractController{
    //#[Route('/Usuario', name: 'usuario')]

    //public function index (){
    //    return $this->render('Usuario/usuario.html.twig');
    //}

    public function getUsuario (UsuarioRepository $us){
        
       
        $listUsers = $us->findAll();
        return $this-> render('usuario.html.twig',[
            'listusuario' => $listUsers
        ]);
    }
}